## Pagina web Brazos abiertos

Esta pagina web esta dedicada para la informacion y difusion de noticias hacia diferentes paises.
La pagina tiene 2 versiones , ingles y español.

## Bosquejo de la pagina principal

![img](https://bytebucket.org/jesteban19/brazos-abiertos-web/raw/aa4db62b040386019fb9fe03777d0a6d6c218918/tools/home.png)

## Paleta de colores
![img](https://bytebucket.org/jesteban19/brazos-abiertos-web/raw/aa4db62b040386019fb9fe03777d0a6d6c218918/tools/colors.jpg)

Puedes visitar la pagina web en **[BrazosAbiertos.net](http://brazosabiertos.net)**